//Copyright 2017 (c) SmartIT. All rights reserved. By John Kocer
using vit.DataAccess.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using vit.Data;
using vit.Entities;
using Microsoft.EntityFrameworkCore;

namespace vit.DataAccess.Classes
{
    public class VillageMembersRepository : GenericRepository<VillageMembers>, IVillageMembersRepository
    {
        public VillageMembersRepository(VITContext context) : base(context) { }

        public override VillageMembers Get(int Id)
        {
            return GetAll().Include(c => c.Parent).FirstOrDefault(b => b.Id == Id);
        }
    }
}
