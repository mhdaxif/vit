
using vit.Entities;

namespace vit.DataAccess.Interfaces
{
    public interface IVillageMembersRepository : IGenericRepository<VillageMembers>
    {
        new VillageMembers Get(int id);
    }
}
 