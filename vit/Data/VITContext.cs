﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using vit.Models;
using vit.Entities;

namespace vit.Data
{
    public class VITContext : IdentityDbContext<ApplicationUser>
    {
        public VITContext(DbContextOptions<VITContext> options) : base(options) { }

        public virtual DbSet<Genders> Genders { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Schools> Schools { get; set; }
        public virtual DbSet<StoreProducts> StoreProducts { get; set; }
        public virtual DbSet<Stores> Stores { get; set; }

        public virtual DbSet<VillageMembers> VillageMembers { get; set; } 


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Genders>(entity =>
            {
                entity.Property(e => e.GenderName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<Schools>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Schools)
                    .HasForeignKey(d => d.GenderId)
                    .HasConstraintName("FK_Schools_Genders");
            });

            modelBuilder.Entity<StoreProducts>(entity =>
            {
                entity.HasOne(d => d.Product)
                    .WithMany(p => p.StoreProducts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoreProducts_Products");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.StoreProducts)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoreProducts_Stores");
            });

            modelBuilder.Entity<Stores>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ShopKeeper).HasMaxLength(250);

                entity.Property(e => e.StoreName).HasMaxLength(500);
            });

            modelBuilder.Entity<VillageMembers>(entity =>
            {
                entity.Property(e => e.CellPhone).HasMaxLength(50);

                entity.Property(e => e.Education).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Profession).HasMaxLength(500);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_VillageMembers_VillageMembers");
            });
        }
    }
}
