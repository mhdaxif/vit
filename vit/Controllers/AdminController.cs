﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using vit.DataAccess.Interfaces;
using vit.ViewModels;
using vit.Entities;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace vit.Controllers
{
    public class AdminController : BaseController
    {
        private IVillageMembersRepository _member;
        private ILogger<AdminController> _logger;
        private IMapper _mapper;

        public AdminController(IVillageMembersRepository member,
                                ILogger<AdminController> logger,
                                IMapper mapper)
        {
            _member = member;
            _logger = logger;
            _mapper = mapper;
        }

        // HttGet // return all the members
        public ActionResult Index()
        {
            var model = _member.GetAllIncluding(x => x.Parent).ToList();
            return View(model);
        }

        public ActionResult EditMember(int id)
        {
            var member = _member.Get(id);
            var parents = _member.GetAll().ToList();
            ViewBag.ParentId = new SelectList(parents, "Id", "Name");
            if (member == null)
            { 
                return RedirectToAction("Index");
            }
            return View(member);
        }

        [HttpPost]
        public ActionResult UpdateMember(VillageMembers member)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _member.Update(member);
                    _member.Commit(); 

                    return RedirectToAction("Index");
                }
                return View("EditMember", member);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Trow and exeception : {ex.Message}");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult CreateMember()
        {
            var parents = _member.GetAll().ToList();
            ViewBag.ParentId = new SelectList(parents, "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult CreateMember(VillageMembers member)
        {
            try
            {
                if (ModelState.IsValid) 
                {
                  var item =  _member.Add(member);
                    _member.Commit();
                    return RedirectToAction("Index");
                }
                return View(member);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Trow and exeception : {ex}");
            }

            return View(member);
        }


     
        public ActionResult DeleteMember(int id)
        {
            try 
            {
                var member = _member.GetSingle(x => x.Id == id);

                if (member == null)
                {
                    TempData["Message"] = $"No Item found {id}";
                    return RedirectToAction("Index");
                }

                _member.Delete(member);
                _member.Commit();

               return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Trow and exeception : {ex}");
            }

            return RedirectToAction("Index");
        } 

        public ActionResult MemberDetails(int id)
        {
            try
            {
                var member = _member.Get(id);

                if (member == null)
                {
                    TempData["Message"] = $"No Item found {id}";
                    return RedirectToAction("Index");
                }

                return View(member);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Trow and exeception : {ex}");
            }

            return RedirectToAction("Index");
        } 
    }
}
