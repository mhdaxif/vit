﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vit.Data;
using vit.DataAccess.Interfaces;
using vit.ViewModels;
using vit.Entities;

namespace vit.Controllers
{
    [Route("[controller]/[action]")]
    public class MembersController : Controller
    {
        private IVillageMembersRepository _repo;

        public MembersController(IVillageMembersRepository repo)
        {
            _repo = repo;  
        }
        public ActionResult Index(string searchby, string Search)
        {
            var model = _repo.GetAllIncluding(x => x.Parent);

            if (searchby == null && Search == null)
            {
                return View(model);
            }

            switch (searchby)
            {
                case "Name":
                    model = model.Where(x => x.Name.Contains(Search));
                    return View(model);
                case "Age":
                    model = model.Where(x => x.Age.ToString() == Search);
                    return View(model); 
                case "Education":
                    model = model.Where(x => x.Education.Contains(Search));
                    return View(model);
            }

            return View(model);
        }
    }
}
 