﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using vit.DataAccess.Interfaces;
using vit.ViewModels;
using vit.Models; 

namespace vit.Controllers
{
    public class HomeController : BaseController
    {
        private IVillageMembersRepository _repo;

        public HomeController(IVillageMembersRepository repo) 
        {
            _repo = repo; 
        } 
        public IActionResult Index()
        {
            var model = _repo.GetAll()
                        .Where(x => x.ParentId == null)
                        .Select(c => new MembersViewModel
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Age = c.Age,
                            Avatar = c.Avatar,
                            CellPhone = c.CellPhone,
                            Education = c.Education,
                            IsAlive = c.IsAlive,
                            MaritialStatus = c.MaritialStatus,
                            Parent = c.Parent, 
                            Profession = c.Profession
                        }).ToList(); 
            var FamilyViewModel = CreateModel(model);
            FamilyViewModel.TotalChildren = model.Count;
            return View(FamilyViewModel); 
        }

        // Family detatils
        public IActionResult GetFamilyInfo(int? id) 
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var children = _repo.GetAllIncluding(x => x.Parent)
                            .Where(c => c.ParentId == id)
                            .Select(c => new MembersViewModel{
                                Id = c.Id,
                                Name = c.Name,
                                Age = c.Age,
                                Avatar = c.Avatar,
                                CellPhone = c.CellPhone,
                                Education = c.Education,
                                IsAlive = c.IsAlive,
                                MaritialStatus = c.MaritialStatus,
                                Parent = c.Parent,
                                ParentId = c.ParentId,
                                Profession = c.Profession

                            }).ToList();

            var parent = _repo.GetAll().FirstOrDefault(x => x.Id == id);

            var FamilyViewModel = CreateModel(children);
            if (parent != null)
            {
                FamilyViewModel.Name = parent.Name;
                FamilyViewModel.ParentId = parent.ParentId; 
            }
             
            return View(FamilyViewModel);
        }


# region Helpers
        [NonAction]
        public FamilyViewModel CreateModel(List<MembersViewModel> children)
        {
            foreach (var item in children)
            {
                var operationResult = GetMemberInfo(item.Id);
                item.HaveKids = operationResult.HaveKids;
                item.TotalKids = operationResult.TotalKids;
            } 

            var FamilyViewModel = new FamilyViewModel
            {
                Members = children,
                TotalChildren = children.Count,
            };
            return FamilyViewModel;
        }

        [NonAction]
        public MemberInfo GetMemberInfo(int? id)
        {
            var totalKids = _repo.GetAll().Where(x => x.ParentId == id).ToList().Count;
            var haveKids = (totalKids > 0) ? true : false;
            return new MemberInfo() { HaveKids = haveKids, TotalKids = totalKids };
        } 

        #endregion
    }
}