﻿using AutoMapper;
using vit.Entities;
using vit.ViewModels;

namespace vit.Models
{
    public class AdminMappingProfile : Profile
    {
        public AdminMappingProfile()
        {
            CreateMap<VillageMembers, MembersViewModel>().ReverseMap();
        }
    }
}
