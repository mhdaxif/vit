﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class VillageMembers
    {
        public VillageMembers()
        {
            InverseParent = new HashSet<VillageMembers>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Age { get; set; }
        public string Education { get; set; }
        public string CellPhone { get; set; }
        public string Profession { get; set; }
        public string Avatar { get; set; }
        public bool? MaritialStatus { get; set; }
        public bool? IsAlive { get; set; }

        public VillageMembers Parent { get; set; }
        public ICollection<VillageMembers> InverseParent { get; set; }
    }
}
