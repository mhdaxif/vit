﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class Genders
    {
        public Genders()
        {
            Schools = new HashSet<Schools>();
        }

        public int Id { get; set; }
        public string GenderName { get; set; }

        public ICollection<Schools> Schools { get; set; }
    }
}
