﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class Products
    {
        public Products()
        {
            StoreProducts = new HashSet<StoreProducts>();
        }

        public int Id { get; set; }
        public string ProductName { get; set; }
        public int? Quatity { get; set; }
        public string Description { get; set; }

        public ICollection<StoreProducts> StoreProducts { get; set; }
    }
}
