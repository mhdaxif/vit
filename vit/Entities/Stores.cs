﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class Stores
    {
        public Stores()
        {
            StoreProducts = new HashSet<StoreProducts>();
        }

        public int Id { get; set; }
        public string StoreName { get; set; }
        public string ShopKeeper { get; set; }
        public bool? IsActive { get; set; }

        public ICollection<StoreProducts> StoreProducts { get; set; }
    }
}
