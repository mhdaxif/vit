﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class Schools
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? GenderId { get; set; }
        public bool? IsActive { get; set; }

        public Genders Gender { get; set; }
    }
}
