﻿using System;
using System.Collections.Generic;

namespace vit.Entities
{
    public partial class StoreProducts
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int ProductId { get; set; }

        public Products Product { get; set; }
        public Stores Store { get; set; }
    }
}
