﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vit.Entities;

namespace vit.ViewModels
{
    public class FamilyViewModel
    {
        public FamilyViewModel()
        {
            Members = new List<MembersViewModel>();
        }
        public List<MembersViewModel> Members { get; set; }
        public int TotalChildren { get; set; }
        public string  Name { get; set; }
        public int? ParentId { get;  set; }
    }
} 
 