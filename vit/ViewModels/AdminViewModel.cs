﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vit.Entities;

namespace vit.ViewModels
{
    public class MemberEditViewModel 
    { 
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Age { get; set; }
        public string Education { get; set; }
        public string CellPhone { get; set; }
        public string Profession { get; set; }
        public string Avatar { get; set; }
        public bool? MaritialStatus { get; set; }
        public bool? IsAlive { get; set; }
        public VillageMembers Parent { get; set; } 

    } 
}
